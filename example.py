"""Демонстрация возможных функций библиотеки ehttp"""
import ehttp


server = ehttp.Server(
    host="localhost",  # адресс, на котором будет запущен сервер
    port=3000,  # порт
    force_bind=False,  # принудительно завершить процесс если он занимает нужный порт
    default_headers=None  # заголовки, которые всегда будет возвращать сервер
)


@server.bind("/index")
def index(req: ehttp.Request):
    return """
    <b>
        hello
    </b> 
    <i>
        world
    </i>"""


@server.bind(r'/regex/(\w*)(?:\.)(\w*)(?:|/)')
def regex(req: ehttp.Request, first, second):
    return ehttp.Response(
        200, f"""\t\t{req.path}
        \t{first} - {second}
        """,
        content_type="txt"
    )


@server.ebind('/ebind/<first>.<second>')
def ebind(req: ehttp.Request, first, second):
    return ehttp.Response(
        200, f"""\t\t{req.path}
        \t{first} - {second}
        """,
        content_type="txt"
    )


@server.bind("/info")
def info(req: ehttp.Request):
    return req.__dict__


@server.bind("/git")
def git(_):
    return ehttp.File(".gitignore")


@server.bind("/readme")
def readme(_):
    return ehttp.File("README.md")


@server.ebind('/div/<first>&<second>')
def div(_, first, second):
    return float(first)/float(second)

@server.bind("/params")
def params(_:ehttp.Request):
    print(_.path_params)
users = {"pas$w0rd": "wex"}


def check_auth(req: ehttp.Request):
    if req.headers.get("password") in users:
        return users[req.headers["password"]]
    else:
        return ehttp.Response(401, "Неверный пароль")


@server.bind("/auth", check_auth)
def auth(_:ehttp.Request):
    return _.auth

@server.error404()
def e404(req):
    return ehttp.Response(200, "not found")
server.start()
